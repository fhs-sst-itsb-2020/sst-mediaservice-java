package fhs.sst.mediastorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@EnableAsync
@SpringBootApplication
@EnableEurekaClient
public class MediaStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediaStorageApplication.class, args);
    }

}
