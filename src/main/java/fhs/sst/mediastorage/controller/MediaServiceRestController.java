package fhs.sst.mediastorage.controller;

import fhs.sst.mediastorage.domain.Song;
import fhs.sst.mediastorage.service.MediaStorageService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("mediastorage/api/Songs")
public class MediaServiceRestController {
    private MediaStorageService mediaStorageService;

    public MediaServiceRestController(MediaStorageService mediaStorageService) {
        this.mediaStorageService = mediaStorageService;
    }

    @PostMapping
    public Long create(@RequestBody Song song){
        return mediaStorageService.createSong(song);
    }

    @DeleteMapping("{songId}")
    public boolean delete(@PathVariable Long songId){
        return mediaStorageService.deleteSong(songId);
    }

    @GetMapping("{songId}")
    public String getSong(@PathVariable Long songId){
        return mediaStorageService.getSong(songId);
    }

    @PutMapping
    public Song updateSong(Song song){
        return mediaStorageService.updateSong(song);
    }
}
