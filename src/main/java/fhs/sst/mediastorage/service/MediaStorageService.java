package fhs.sst.mediastorage.service;

import fhs.sst.mediastorage.domain.Song;
import fhs.sst.mediastorage.repository.SongRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class MediaStorageService {

    private SongRepository songRepository;

    public MediaStorageService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    public Song updateSong(Song song) {
        return songRepository.save(song);
    }

    public String getSong(Long songId) {
        Optional<Song> song = songRepository.findById(songId);
        if (song.isPresent()){
            return song.get().getBlobString();
        }
        throw new EntityNotFoundException("Song with id "+ songId + "could not be found!");
    }

    public boolean deleteSong(Long songId) {
        songRepository.deleteById(songId);
        return true;
    }

    public Long createSong(Song song) {
        return songRepository.save(song).getId();
    }
}
